module.exports = function(grunt) {
  'use strict';

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    watch: {
      src: {
        files: [ './app.js', './src/**/*.js' ],
        tasks: [ 'jshint', 'injector' ],
        options: {
          spawn: true,
          livereload: 35730
        }
      },

      scss: {
        files: [ 'assets/styles/scss/**/*.scss', 'src/**/*.scss' ],
        tasks: [ 'concat:sass', 'sass' ]
      }
    },

    connect: {
      server: {
        options: {
          port: 4000,
          hostname: 'localhost'
        }
      }
    },

    concat: {
      sass: {
        files: {
          'assets/styles/main.scss': [ 'assets/styles/scss/**/*.scss', 'src/**/*.scss' ]
        }
      }
    },

    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'assets/styles/main.css': 'assets/styles/main.scss'
        }
      }
    },

    jshint: {
      options: {
        jshintrc: true
      },

      all: [ 'src/**/*.js' ]
    },

    injector: {
      options: {
        addRootSlash: false
      },

      app: {
        files: {
          'index.html': [ 'bower.json', 'src/*.js', 'src/*/**/*.js', 'assets/**/*.css' ]
        }
      }
    },
  });

  grunt.registerTask('default', [ 'concat:sass', 'sass', 'jshint', 'injector' ]);
  grunt.registerTask('serve', [ 'connect', 'watch' ]);
};