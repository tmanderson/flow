'use strict';

const electron = require('electron');
const app = electron.app;

const BrowserWindow = electron.BrowserWindow;
const globalShortcut = electron.globalShortcut;
const Menu = electron.Menu;

electron.crashReporter.start();

var mainWindow = null;

app.on('window-all-closed', function() {
  if(process.platform != 'darwin') app.quit();
});

app.on('ready', function() {

  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    center: true,
    alwaysOnTop: true,
    frame: false,
    webPreferences: {
      webSecurity: false
    }
  });

  mainWindow.loadURL('file://' + __dirname + '/index.html');
  
  mainWindow.on('closed', function() {
    mainWindow = null;
  });

  var ret = globalShortcut.register('alt+1', function() {
    if(mainWindow.isVisible()) {
      mainWindow.hide();
      Menu.sendActionToFirstResponder('hide:');
    }
    else {
      mainWindow.show();
    }
  });

  if(!ret) {
    console.log('registration failed');
  }
});

app.on('will-quit', function() {
  // Unregister all shortcuts.
  globalShortcut.unregisterAll();
});