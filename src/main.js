'use strict';

angular.module('dex', [
  'ngCookies',
  'ngResource',
  'ngMaterial',
  'ngSanitize',
  'ngAnimate',
  'ui.router',
  'cfp.hotkeys'
])
.factory('ipc', function() { return require('electron').ipcRenderer; })
.config(function($urlRouterProvider, $stateProvider, $httpProvider) {

  _.mixin({
    setScope: function($scope, prop) {
      console.log(prop);
      return _.partial(_.set, $scope, prop);
    },

    thenSet: function(prop, onObj, def) {
      return _.partial(_.set, onObj, prop, def);
    }
  });

  $urlRouterProvider.otherwise('/');

  // USE SUB VIEWS, MAKE A SEPARATE CONTROLLER BUT WE NEED COMMUNICATION BETWEEN THE MODULES!
  // There should be a state that is the hub of communication (and keeps all in sync)
  // so when editinf from the web view, a selector is added, pass it to the state
  // the sidebar reads the state and displays the selector
  $stateProvider
  .state('category', {
    url: '/',
    controller: 'CategoryController',

    data: {
      toolbar: {
        templateUrl: 'src/category/category.toolbar.template.html'
      },
      sidebar: {
        templateUrl: 'src/category/category.sidebar.template.html'
      },
      content: {
        templateUrl: 'src/category/category.content.template.html'
      }
    }
  });
  
  $httpProvider.interceptors.push('HTTPHostInterceptor');
})
.factory('HTTPHostInterceptor', function($injector) {
  return {
    request: function(req) {
      if(req.url.indexOf('{host}') > -1) {
        req.url = req.url.replace('{host}', $injector.get('API').host);
      }
      return req;
    }
  };
})
.factory('history', function() {
  var history = [];
  
  _.extend(history.prototype, {
    last: _.partial(_.last, history),
    all : _.partial(_.identity, history),
  });

  return history;
})
.run(function($rootScope) {
  $rootScope.$on('$stateChangeStart', function(e, toState){ 
    _.extend($rootScope, _.defaults(toState.data, { toolbar: {}, sidebar: {}, content: {} }));
    console.log(toState);
  });
});