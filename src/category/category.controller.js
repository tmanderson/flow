'use strict';

angular.module('dex')
.controller('CategoryController', function($scope, hotkeys) {
  console.log('HERE!');
  $scope.categories = [
    {
      name: 'Documentation',
      sources: [
        {
          name: 'Electron (Atom)',
          host: 'electron.atom.io',
          url: 'http://electron.atom.io/docs/v0.36.4/'
        }
      ]
    },
    {
      name: 'News',
      sources: [
        {
          name: 'New York Times',
          url: 'http://nytimes.com',
          host: 'nytimes.com'
        },
        {
          url: 'https://washingtonpost.com',
          name: 'The Washington Post',
          host: 'washingtonpost.com'
        },
        {
          url: 'http://thenation.com',
          name: 'The Nation',
          host: 'www.thenation.com'
        },
        {
          url: 'http://newyorker.com',
          name: 'The New Yorker',
          host: 'newyorker.com'
        },
        {
          url: 'http://www.theatlantic.com',
          name: 'The Atlantic',
          host: 'theatlantic.com'
        },
        {
          url: 'http://www.theguardian.com/us',
          name: 'The Guardian US',
          host: 'theguardian.com'
        },
        {
          url: 'http://www.dnainfo.com/chicago/',
          name: 'DNA Info – Chicago',
          host: 'dnainfo.com'
        },
        {
          url: 'https://www.jacobinmag.com/',
          name: 'Jacobin',
          host: 'jacobinmag.com'
        },
        {
          url: 'http://www.chicagotribune.com/',
          name: 'Chicago Tribune',
          host: 'chicagotribune.com'
        },
        {
          url: 'http://www.politico.com/',
          name: 'Politico',
          host: 'www.politico.com'
        },
        {
          url: 'https://newrepublic.com/',
          name: 'New Republic',
          host: 'newrepublic.com'
        }
      ]
    }
  ];

  $scope.showCategory = function(category) {
    if(!category) return;
    _.each($scope.categories, function(c) { delete c.active; });
    category.active = true;
    $scope.category = category;
    $scope.showSource(_.find(category.sources, 'active', true));
  };

  $scope.showSource = function(source) {
    if(!source) return;
    _.each($scope.category.sources, function(s) { delete s.active; });
    source.active = true;
    var webview = document.querySelector('webview');
    webview.src = source.url;
    
    webview.addEventListener('console-message', function(e) {
      console.log(e.message);
    });

  };

  $scope.startSearch = function() {
    document.querySelector('input').focus();
  };

  $scope.search = function(query) {
    if(!query) return;
    var webview = document.querySelector('webview');
    webview.findInPage(query);
  };

  $scope.stopSearch = function() {
    var webview = document.querySelector('webview');
    webview.stopFindInPage('keepSelection');
  };

  $scope.toggleContentEditor = function() {
    document.querySelector('webview').executeJavaScript('DEX.toggle()');
  };

  hotkeys.add({
    combo: 'down',
    callback: function() {
      var category = _.find($scope.categories, 'active', true);
      var active = _.findIndex(category.sources, 'active', true);

      $scope.showSource(category.sources[Math.min(category.sources.length-1, Math.max(0, ++active))]);
    }
  });

  hotkeys.add({
    combo: 'up',
    callback: function() {
      var category = _.find($scope.categories, 'active', true);
      var active = _.findIndex(category.sources, 'active', true);

      $scope.showSource(category.sources[Math.max(0, Math.min(category.sources.length-1, --active))]);
    }
  });

  hotkeys.add({
    combo: 'meta+f',
    action: 'keydown',
    callback: $scope.startSearch.bind($scope)
  });

  hotkeys.add({
    combo: '1',
    callback: $scope.showCategory.bind($scope, $scope.categories[0])
  });

  hotkeys.add({
    combo: '2',
    callback: $scope.showCategory.bind($scope, $scope.categories[1])
  });

  
});