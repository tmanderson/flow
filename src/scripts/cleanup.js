/**
  Looking to select elements in order to "filter" the content
  for easy viewing for users.

  This enables a configuration of sorts. So the user can choose which content
  they want to see for the websites, and this persists. Possibly allowing
  them to view it in various fashions
**/
(function() {
  'use strict';
  
  var active = false;
  var blocked = false;

  var hierarchy = [];
  var elements = [];
  var notifications = [];

  var statusBar = document.createElement('div');
  statusBar.classList.add('dex-status-bar');

  var styles = document.createElement('style');

  styles.innerText = [
    '*[data-dex-inspect="true"] { border: 1px solid red; background-color: yellow; }',
    '*[data-dex-depth="0"] { border: 1px solid red; }',
    '*[data-dex-depth="1"] { border: 1px solid orange; }',
    '*[data-dex-depth="2"] { border: 1px solid blue; }',
    '.dex-notification { position: fixed; right: 20px; opacity: 0; transition: opacity 400ms ease 200ms; min-height: 50px; width: 300px; background-color: #FFEC0C; color: #333; z-index: 9998; padding: 10px; }',
    '.dex-notification.show { opacity: 1; }',
    '.dex-status-bar { padding: 0 20px; line-height: 1.6em; min-height: 1.6em; position: fixed; bottom: 0; left: 0; right: 0; z-index: 9999; background-color: #333; color: #fff; font-weight: bold; }'
  ].join('\n');

  function createNotification(msg) {
    var note = document.createElement('div');
    var text = document.createElement('strong');
    note.setAttribute('class', 'dex-notification');
    text.innerText = msg;
    note.appendChild(text);
    notifications.push(note);
    return note;
  }

  function getNotificationOffset() {
    return notifications.reduce(function(bot, el) {
      bot += el.offsetHeight + 20;
      return bot;
    }, 10);
  }
  
  function notify(msg) {
    var notification = createNotification(msg);
    notification.style.bottom = getNotificationOffset() + 'px';
    document.body.appendChild(notification);
    notification.classList.add('show');
    setTimeout(removeNotification.bind(null, notification), 5000);
  }

  function removeNotification(notification) {
    notification.classList.remove('show');

    setTimeout(function() {
      notifications.splice(notifications.indexOf(notification), 1);
      notification.parentNode.removeChild(notification);  
    }, 500);
  }

  function setStatus(text) {
    statusBar.innerText = text;
  }

  function removeSelected(from) {
    Array.prototype.forEach.call(
      from || document.querySelectorAll("[data-dex-inspect]"),
      function(el) { el.removeAttribute("data-dex-inspect"); }
    );
  }

  function getSelector(el) {
    var cls = el.getAttribute("class");
    return el.tagName.toLowerCase() + (cls ? '.' + cls.replace(/\s+/g, '.') : '');
  }

  function getContainerSelector() {
    return hierarchy.reduce(function(sel, el) { return sel += getSelector(el); }, '');
  }

  function matches(el, selector) {
    return (typeof selector === 'function' ? selector(el) : el.matches(selector));
  }

  function getClosest(selector, el) {
    var target = el;

    while(target && target.parentNode) {
      if(matches(target, selector)) return el;
      target = target.parentNode;
    }
  }

  function toggleBlock(e) {
    blocked = !blocked;
  }

  function addToHierarchy(e) {
    var i;
    e.preventDefault();
    e.stopPropagation();
    
    if(e.target.getAttribute('data-dex-depth')) {
      i = hierarchy.indexOf(e.target);
      e.target.removeAttribute('data-dex-depth');
      hierarchy.splice(i, hierarchy.length - i);
    }
    else {
      e.target.setAttribute('data-dex-depth', hierarchy.length);  
      hierarchy.push(e.target);
    }
  }

  function highlightElement(e) {
    if(blocked) return;

    var target, sel = getContainerSelector();

    setStatus(sel);
    removeSelected();

    target = getClosest(function(el) {
      return (window.getComputedStyle(el, null).display === "block");
    }, e.target);

    target.setAttribute('data-dex-inspect', true);

    if(!target || !target.parentNode) return;
    
    sel += (sel.length ? ' ' : '') + getSelector(target);
    
    setStatus(sel);

    if(sel) {
      elements = document.querySelectorAll(sel);

      Array.prototype.forEach.call(elements, function(el) {
        el.setAttribute("data-dex-inspect", true);
      });
    }
  }

  function enableContentSelector() {
    document.body.appendChild(styles);
    document.body.appendChild(statusBar);
    document.addEventListener('mouseover', highlightElement, true);
    document.addEventListener('click', addToHierarchy, true);
    document.addEventListener('dblclick', toggleBlock, true);
    active = true;
    notify('DEX active!');
  }

  function disableContentSelector() {
    document.body.removeChild(styles);
    statusBar.parentNode.removeChild(statusBar);
    document.removeEventListener('mouseover', highlightElement, true);
    document.removeEventListener('click', addToHierarchy, true);
    document.removeEventListener('dblclick', toggleBlock, true);
    active = false;
    notify('DEX inactive!!');
  }

  window.DEX = {
    toggle: function() {
      console.log(!active ? 'ENABLED' : 'DISABLED');
      return (active ? disableContentSelector() : enableContentSelector());
    }
  };
})();