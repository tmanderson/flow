## Group

- Default "Main Content"
  - Blank
  - Display Source(s)

### Sources
- Can be grouped and sub-grouped (if selected, all display) into categories

### Source Display
- Website
- Parse and format (custom styles)

### Type

## Source
- Schemas basically form the "data" available from a source
- "Templates" would include pre-named properties that are "filled-in" by choosing elements from the given source URL

### Templates (Properties)
- "article" (see schema.org `article`)
- "documentation"

Values to properties are selectors that are retrieved through the interactive selection editor. If a selector resolves to a link, the property can be assigned to another schema (that handles the link, via `handler`)

```json
{
    "host": "http://something.com",
    "name": "Something", // (optional, use `<title>`)
    "schemas": [
        {
            "id": "#", // (json-schema.org, root schema)
            "type": "article", // (schema.org name),
            "rel": "#full", // (reference schema)
            "props": {
                // (required with "rel" via json-schema.org)
                "href": ".some a.selector-to-article", 
                
                "title": ".some h1.selector",
                "date": ".some span.selector"
            } 
        },
        {
            "id": "#full",
            "props": { ... }
        }
    ]
}
```
